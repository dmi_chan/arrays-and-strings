public class Arrays {
 
    public static void main(String[] args) {
    	double arr[] = new double[5]; 
    	arr[1] = 10.0;
    	arr[2] = 4.0;
    	arr[3] = 1.0;
    	arr[4] = 2.0;
    	
        double buf;
        double d = 0.1;
        
        for (int  i = 0; i < arr.length; i++) {
        	
        	arr[i] =arr[i] * 1.1;
        }
        
        for (int i = arr.length-1; i > 0; i--) {
        	for (int j = 0; j < i; j++) {
        		if (arr[j] > arr[j + 1]) {
        			double a = arr[j];
        			arr[j] = arr[j + 1];
        			arr[j + 1] = a;
                    }
                }
            }
        for (int i = 0; i < arr.length; i++) {
        	System.out.print(arr[i]+" ");
        }
}
}